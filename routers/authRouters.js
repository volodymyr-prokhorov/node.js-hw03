const express = require('express');

const router = express.Router();
const { registerUser, loginUser, restorePassword } = require('../services/authServices');

router.post('/register', registerUser);

router.post('/login', loginUser);

router.post('/forgot_password', restorePassword);

module.exports = {
  authRouters: router,
};
