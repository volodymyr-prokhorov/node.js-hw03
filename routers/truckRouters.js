const express = require('express');

const router = express.Router();
const {
  getUserTrucks,
  addUserTrucks,
  getUserTrucksById,
  updateUserTrucksById,
  deleteUserTrucksById,
  assignTruckToUserById,
} = require('../services/truckServices');

router.get('/', getUserTrucks);

router.post('/', addUserTrucks);

router.get('/:id', getUserTrucksById);

router.put('/:id', updateUserTrucksById);

router.delete('/:id', deleteUserTrucksById);

router.post('/:id/assign', assignTruckToUserById);

module.exports = {
  truckRouters: router,
};
