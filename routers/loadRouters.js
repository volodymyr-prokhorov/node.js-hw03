const express = require('express');

const router = express.Router();
const {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getLoadShippingInfo,
} = require('../services/loadServices');

router.get('/', getUserLoads);

router.post('/', addUserLoad);

router.get('/active', getUserActiveLoad);

router.patch('/active/state', iterateLoadState);

router.get('/:id', getUserLoadById);

router.put('/:id', updateUserLoadById);

router.delete('/:id', deleteUserLoadById);

router.post('/:id/post', postUserLoadById);

router.get('/:id/shipping_info', getLoadShippingInfo);

module.exports = {
  loadRouters: router,
};
