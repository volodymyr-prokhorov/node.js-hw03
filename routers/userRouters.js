const express = require('express');

const router = express.Router();
const {
  getUserProfileInfo,
  deleteUserProfile,
  changeUserPassword,
} = require('../services/userServices');

router.get('/', getUserProfileInfo);

router.delete('/', deleteUserProfile);

router.patch('/password', changeUserPassword);

module.exports = {
  userRouters: router,
};
