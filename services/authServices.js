//  Authentification

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

const registerUser = async (req, res, next) => {
  const { email, password, role } = req.body;

  if (!email || !password || !role) {
    return res.status(400).json({ messge: 'Email or password is empty. Please provide correct data.' });
  }

  const user = new User({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  user.save()
    .then((saved) => {
      res.status(200).json({ message: 'Success' });
    })
    .catch((err) => {
      next(err);
    });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ email: req.body.email });
  if (user && await bcrypt.compare(String(req.body.password), String(user.password))) {
    const payload = { email: user.email, userId: user._id, role: user.role };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');
    return res.json({ message: 'Success', jwt_token: jwtToken });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

const restorePassword = async (req, res, next) => {
  const result = await User.exists({ email: req.body.email });
  if (!result) {
    return res.status(400).json({ message: 'No such account. Please enter the correct email.' });
  }
  return res.status(200).json({ message: 'New password sent to your email address' });
};

module.exports = {
  registerUser,
  loginUser,
  restorePassword,
};
