const { Load } = require('../models/loadModel');
const { Track } = require('../models/truckModel');

// Retrieve the list of loads for authorized user,
// returns list of completed and active loads for Driver
// and list of all available loads for Shipper
function getUserLoads(req, res, next) {
  const filter1 = [{ created_by: req.user.userId }, { assigned_to: req.user.userId }];
  const filter2 = { status: req.query.status };
  return Load.find({ $or: filter1, filter2 })
    .skip(req.query.offset).limit(req.query.limit)
    .then((result) => {
      res.json({
        offset: req.query.offset,
        limit: req.query.limit,
        count: result.length,
        loads: result,
      });
    });
}

// Add Load for User(abailable only for shipper role)
function addUserLoad(req, res, next) {
  if (req.user.role === 'SHIPPER') {
    const {
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    } = req.body;
    const load = new Load({
      created_by: req.user.userId,
      assigned_to: null,
      status: 'NEW',
      state: null,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      logs: [{
        message: `Load is created by shipper id: ${req.user.userId}`,
        time: Date.now(),
      }],
    });
    return load.save().then((saved) => {
      res.json({ message: 'Load created successfully' });
    });
  }
  return res.status(400).json({ message: 'You do not have permission to register Loads' });
}

// Retrieve the active load for authorized driver (available only for driver)
function getUserActiveLoad(req, res, next) {
  if (req.user.role === 'DRIVER') {
    return Load.findOne({ assigned_to: req.user.userId, status: 'ASSIGNED' }, '-__v')
      .then((result) => {
        // if (result === null)
        res.json({
          load: result,
        });
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Iterate to next Load state(available only for driver)
// Once Load state changed to 'Arrived to delivery'
// Load status changes to 'SHIPPED'
// Track status changed to 'IS'
async function iterateLoadState(req, res, next) {
  const stateList = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];

  if (req.user.role === 'DRIVER') {
    const activeLoad = await Load.findOne({ assigned_to: req.user.userId, status: 'ASSIGNED' }, '-__v');
    const activeTrack = await Track.findOne({ assigned_to: req.user.userId });
    if (activeLoad === null || activeTrack === null) {
      return res.status(400).json({ message: 'Oops, something goes wrong.' });
    }
    const index = stateList.indexOf(activeLoad.state);
    if (index >= 0 && index < stateList.length - 2) {
      const nextState = stateList[index + 1];
      activeLoad.state = nextState;
      activeLoad.logs.push({ message: `Load state changed to: ${nextState}`, time: Date.now() });
    } else {
      activeLoad.status = 'SHIPPED';
      activeTrack.status = 'IS';
    }
    activeLoad.save((err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
      }
    });
    activeTrack.save((err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
      }
    });
    // return res.json({ activeLoad, activeTrack });
    // return activeLoad.save().then((saved) => res.json({ saved }));
    // return res.json({ message: 'Load state changed to "En route to Delivery"' });
    return res.json({ message: `Load state changed to: ${activeLoad.state}` });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Get user's Load by id
function getUserLoadById(req, res, next) {
  return Load.findById({ _id: req.params.id, userId: req.user.userId }, '-__v')
    .then((result) => {
      if (result === null) {
        res.json({ message: 'Such an entry does not exist. Please enter the valid id.' });
      } else {
        res.json({ load: result });
      }
    });
}

// TODO Add validation for passed fields. How to do it?
// Update user's load by id  with status 'NEW' (abailable only for shipper role)
async function updateUserLoadById(req, res, next) {
  const update = req.body;
  const filter = { _id: req.params.id, userId: req.user.userId, status: 'NEW' };
  const check = await Load.findOne(filter);
  if (check === null) {
    return res.status(400).json({ message: 'You can`t update Load that has no NEW state' });
  }
  if (req.user.role === 'SHIPPER') {
    const doc = await Load.findByIdAndUpdate(filter, update, {
      new: true,
    });
    if (doc === null) {
      res.json({ message: 'Such an entry does not exist. Please enter the valid id.' });
    } else {
      return res.json({ message: 'Load details changed successfully' });
    }
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Delete user's load by id with status 'NEW' (abailable only for shipper role)
async function deleteUserLoadById(req, res, next) {
  const filter = { _id: req.params.id, userId: req.user.userId, status: 'NEW' };
  const check = await Load.findOne(filter);
  if (check === null) {
    return res.status(400).json({ message: 'You can`t delete Load that has no NEW state' });
  }
  if (req.user.role === 'SHIPPER') {
    return Load.findByIdAndDelete(filter)
      .then((result) => {
        if (result === null) {
          res.json({ message: 'Such an entry does not exist. Please enter the valid id.' });
        } else {
          res.json({ message: 'Load deleted successfully' });
        }
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Post a user's load by id, search for drivers
// (abailable only for shipper role)
async function postUserLoadById(req, res, next) {
  const filter = { _id: req.params.id, userId: req.user.userId, status: 'NEW' };
  const upadate = { status: 'POSTED' };
  if (req.user.role === 'SHIPPER') {
    // 1. Set this Load status to 'POSTED'
    const check = await Load.findOne(filter);
    if (check === null) {
      return res.status(400).json({ message: 'You can`t reassign Load that has no NEW state' });
    }
    const load = await Load.findByIdAndUpdate(filter, upadate, { new: true });
    // console.log(load);
    const { width, length, height } = load.dimensions;
    let requiredTruck = '';
    // 2.1 Compare Load parameters with Trucks, payload > load, dimensions > load size
    if (width < 300 && length < 250 && height < 170 && load.payload < 1700) {
      requiredTruck = [{ type: 'SPRINTER' }, { type: 'SMALL STRAIGHT' }, { type: 'LARGE STRAIGHT' }];
    } else if (width < 500 && length < 250 && height < 170 && load.payload < 2500) {
      requiredTruck = [{ type: 'SMALL STRAIGHT' }, { type: 'LARGE STRAIGHT' }];
    } else if (width < 700 && length < 350 && height < 200 && load.payload < 400) {
      requiredTruck = [{ type: 'LARGE STRAIGHT' }];
    } else {
      return Load.findByIdAndUpdate(filter, { status: 'NEW' }, { new: true })
        .then((result) => {
          res.status(400).json({ message: 'There are no suitable Trucks in the database to transport your Loads. 1' });
        });
    }
    // 2.2 Find a Truck with 'IS' status
    const track = await Track.findOne({ assigned_to: { $ne: null }, $or: requiredTruck, status: { $ne: 'OL' } });
    if (track === null) {
      return Load.findByIdAndUpdate(filter, { status: 'NEW' }, { new: true })
        .then((result) => {
          res.status(400).json({ message: 'There are no suitable Trucks in the database to transport your Loads. 2' });
        });
    }
    // 3.1 Truck staus changes to 'OL'
    track.status = 'OL';
    // 3.2 Load status changes to 'ASSIGNED'
    load.status = 'ASSIGNED';
    // 3.3 Asign load to Driver
    load.assigned_to = track.assigned_to;
    // 3.4 Load state to 'En route to Pick Up'
    load.state = 'En route to Pick Up';
    // 3.5 Add appropriate info to Load logs
    load.logs.push({ message: `Load assigned to driver with id: ${track.assigned_to}`, time: Date.now() });
    // 3.6 Save changes to Database
    load.save((err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
      }
    });
    track.save((err, result) => {
      if (err) {
        console.log(err);
      } else {
        console.log(result);
      }
    });
    // 4. If Driver was not found, Load status roll back to 'NEW'

    // return res.json({ load, track });
    return res.json({ message: 'Load posted successfully', driver_found: true });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Get user's Load shipping info by id,
// returns detailed info about shipment for active loads (available only for shipper)
function getLoadShippingInfo(req, res, next) {
  if (req.user.role === 'SHIPPER') {
    return Load.find({ userId: req.user.userId })
      .then((result) => {
        res.json(result);
      });
    // return res.json({ load: 'Some text...' });
  }
  return res.status(400).json(req.user.role);
  // return res.status(400).json({ message: 'You do not have access to this information' });
}

module.exports = {
  getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getLoadShippingInfo,
};
