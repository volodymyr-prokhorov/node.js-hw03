// User profile
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { User } = require('../models/userModel');

async function getUserProfileInfo(req, res, next) {
  return await User.findById({ _id: req.user.userId })
    .then((user) => {
      if (user != null) {
        res.status(200).json({
          user:
                {
                  _id: req.user.userId,
                  role: user.role,
                  email: user.email,
                  created_date: user.created_date,
                },
        });
      } else {
        return res.status(400).json({ message: 'Oops, something wrong!' });
      }
    });
}

async function deleteUserProfile(req, res, next) {
  return await User.findByIdAndDelete({ _id: req.user.userId })
    .then((result) => {
      res.status(200).json({ message: 'Profile deleted successfully' });
    });
}

const changeUserPassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById({ _id: req.user.userId });

  if (bcrypt.compare(String(newPassword), String(user.password))) {
    user.password = await bcrypt.hash(newPassword, 10);
    return user.save().then((saved) => res.json({ message: 'Password changed successfully' }));
  }
};

module.exports = {
  getUserProfileInfo,
  deleteUserProfile,
  changeUserPassword,
};
