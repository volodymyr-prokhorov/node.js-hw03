const { Track } = require('../models/truckModel');

// Retrieve the list of trucks for authorized user (abailable only for driver role)
function getUserTrucks(req, res, next) {
  console.log(req.user);
  if (req.user.role === 'DRIVER') {
    return Track.find({ created_by: req.user.userId }, '-__v')
      .then((result) => {
        res.json({
          trucks: result,
        });
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// Add Truck for User(abailable only for driver role)
function addUserTrucks(req, res, next) {
  if (req.user.role === 'DRIVER') {
    const { type } = req.body;
    const track = new Track({
      created_by: req.user.userId,
      assigned_to: null,
      type,
      status: 'IS',
    });
    return track.save().then((saved) => {
      res.json({ message: 'Truck created successfully' });
    });
  }
  return res.status(400).json({ message: 'You do not have permission to register Trucks' });
}

// Get user's truck by id(abailable only for driver role)
function getUserTrucksById(req, res, next) {
  if (req.user.role === 'DRIVER') {
    return Track.findById({ _id: req.params.id, userId: req.user.userId }, '-__v')
      .then((truck) => {
        res.json({ truck });
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// TODO Driver is not able to change any profile or his trucks info while he is on load
// Update user's truck by id (abailable only for driver role)
async function updateUserTrucksById(req, res, next) {
  if (req.user.role === 'DRIVER') {
    const { type } = req.body;
    const truck = await Track.findById({ _id: req.params.id, userId: req.user.userId });
    if (truck === null) {
      return res.status(400).json({ message: 'Such an entry does not exist. Please enter the valid id' });
    }
    if (String(truck.created_by) === String(truck.assigned_to) || truck.status === 'OL') {
      return res.status(400).json({ message: 'You are not allowed to update information about the truck assigned to you' });
    }
    truck.type = type;
    return truck.save().then((save) => res.json({ message: 'Truck details changed successfully' }));
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// TODO Driver is not able to change any profile or his trucks info while he is on load
// Delete user's truck by id (abailable only for driver role)
function deleteUserTrucksById(req, res, next) {
  if (req.user.role === 'DRIVER') {
    return Track.findOneAndDelete({ _id: req.params.id, userId: req.user.userId, status: { $ne: 'OL' } })
      .then((result) => {
        if (result === null) {
          res.json({ message: 'Such an entry does not exist. Please enter the valid id.' });
        } else {
          res.json({ message: 'Truck deleted successfully' });
        }
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

// TODO Assign a car to a driver and deselect the previous car.
// TODO Driver is not able to change any profile or his trucks info while he is on load
// Assign truck to user by id (abailable only for driver role)
function assignTruckToUserById(req, res, next) {
  if (req.user.role === 'DRIVER') {
    return Track.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { assigned_to: req.user.userId } },
    )
      .then((result) => {
        if (result === null) {
          res.json({ message: 'Such an entry does not exist. Please enter the valid id.' });
        } else {
          res.json({ message: 'Truck assigned successfully' });
        }
      });
  }
  return res.status(400).json({ message: 'You do not have access to this information' });
}

module.exports = {
  getUserTrucks,
  addUserTrucks,
  getUserTrucksById,
  updateUserTrucksById,
  deleteUserTrucksById,
  assignTruckToUserById,
};
