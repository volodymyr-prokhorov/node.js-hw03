// Track model

const mongoose = require('mongoose');

const trackSchema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});

const collectionName = 'Track';
const Track = mongoose.model('Track', trackSchema, collectionName);

module.exports = {
  Track,
};
