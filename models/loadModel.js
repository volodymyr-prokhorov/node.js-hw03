// Load model

const mongoose = require('mongoose');

const loadShema = mongoose.Schema({
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: { type: mongoose.Schema.Types.ObjectId },
  status: { type: String },
  state: { type: String },
  name: { type: String },
  payload: { type: Number },
  pickup_address: { type: String },
  delivery_address: { type: String },
  dimensions: {
    width: { type: Number },
    length: { type: Number },
    height: { type: Number },
  },
  logs: [{
    message: { type: String },
    time: { type: Date },
  }],
  created_date: {
    type: Date,
    default: Date.now,
  },
});

const collectionName = 'Loads';
const Load = mongoose.model('Load', loadShema, collectionName);

module.exports = {
  Load,
};
