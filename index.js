const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const port = 8080;

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

mongoose.connect('mongodb+srv://vscodeuser:HBnlSPtnaPIXl46c@epam-lab.wjy3sjr.mongodb.net/HW3?retryWrites=true&w=majority');

const { authRouters } = require('./routers/authRouters');
const { userRouters } = require('./routers/userRouters');
const { truckRouters } = require('./routers/truckRouters');
const { loadRouters } = require('./routers/loadRouters');
const { authMiddleware } = require('./middleware/authMiddleware');

app.use('/api/auth/', authRouters);
app.use('/api/users/me', authMiddleware, userRouters);
app.use('/api/trucks', authMiddleware, truckRouters);
app.use('/api/loads', authMiddleware, loadRouters);

const start = async () => {
  try {
    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res, next) {
  console.error('err');
  res.status(500).send({ message: 'Server error' });
}

// ERROR HANDLER
app.use(errorHandler);
